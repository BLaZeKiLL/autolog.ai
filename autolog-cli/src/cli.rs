use clap::{Parser, Subcommand, ValueHint};
use std::path::PathBuf;

#[derive(Clone, Debug, Parser, PartialEq, Eq)]
#[clap(version, about = "A cli tool to build a story for your project")]
pub struct CliOptions {
    #[arg(default_value = ".", hide_default_value = true, value_hint = ValueHint::DirPath)]
    pub project_path: PathBuf,
    #[arg(short, long, default_value = ".auto_log.yaml", value_hint = ValueHint::FilePath)]
    pub config_file_path: PathBuf,
    #[command(subcommand)]
    pub choice: Choice,
}

#[derive(Subcommand, Clone, Debug, PartialEq, Eq)]
pub enum Choice {
    #[command(name = "generate")]
    Generate,
}
