use std::collections::HashMap;

use anyhow::{Ok, Result};
use clap::Parser;
use futures::{stream, StreamExt};
use reqwest::Client;
use std::path::PathBuf;
use tokio;

use git2::{DiffOptions, Oid, Repository};

mod cli;

#[derive(Debug, Clone)]
struct RawGitDiff {
    file_name: String,
    status: git2::Delta,
    diff: Vec<GitDiff>,
}

#[derive(Debug, Clone)]
struct GitDiff {
    action: git2::DiffLineType,
    text: String,
}

#[derive(Debug, Clone)]
struct ParsedGitDiff {
    file_name: String,
    status: git2::Delta,
    old: Vec<String>,
    new: Vec<String>,
}

const CONCURRENT_REQUESTS: usize = 5;

#[tokio::main]
async fn main() -> Result<()> {
    let cli_args = cli::CliOptions::parse();
    match cli_args.choice {
        cli::Choice::Generate => {
            let commit_diff_files = get_code_diff(&cli_args.project_path).unwrap();
            post_request(commit_diff_files).await?;
        }
    }

    Ok(())
}

fn get_code_diff(repo_path: &PathBuf) -> Result<Vec<ParsedGitDiff>> {
    let repo = Repository::open(repo_path).unwrap();

    let mut revwalk = repo.revwalk().expect("Failed to create revwalk");
    revwalk.push_head().expect("Failed to push HEAD");

    let mut merge_pair: Vec<(Oid, Oid)> = Vec::new();
    // Iterate over all commits
    for oid in revwalk {
        let commit_id = oid.expect("Failed to get commit OID");
        let commit = repo.find_commit(commit_id).expect("Failed to find commit");

        if commit.parent_count() > 1 {
            // This is a merge commit, indicating a branch update
            for parent_index in 1..commit.parent_count() {
                let parent_commit = commit
                    .parent(parent_index - 1)
                    .expect("Failed to get parent commit");
                let merged_commit = commit
                    .parent(parent_index)
                    .expect("Failed to get merged commit");

                merge_pair.push((parent_commit.id(), merged_commit.id()));
            }
        }
    }

    let mut raw_git_diffs: Vec<RawGitDiff> = Vec::new();

    for (parent_oid, merged_oid) in merge_pair.iter() {
        let old_commit = repo
            .find_commit(parent_oid.to_owned())
            .expect("Failed to find old commit");
        let new_commit = repo
            .find_commit(merged_oid.to_owned())
            .expect("Failed to find new commit");

        // Retrieve tree objects for the commits
        let old_tree = old_commit.tree().expect("Failed to get old commit tree");
        let new_tree = new_commit.tree().expect("Failed to get new commit tree");

        // Create a diff between the old and new trees
        let mut diff_options = DiffOptions::new();
        let diff = repo
            .diff_tree_to_tree(Some(&old_tree), Some(&new_tree), Some(&mut diff_options))
            .expect("Failed to create diff");

        diff.deltas().for_each(|delta| {
            raw_git_diffs.push(RawGitDiff {
                file_name: delta
                    .new_file()
                    .path()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_owned(),
                status: delta.status(),
                diff: Vec::new(),
            })
        });

        let mut line_callback = |delta: git2::DiffDelta<'_>,
                                 _hunk: Option<git2::DiffHunk<'_>>,
                                 line: git2::DiffLine<'_>| {
            match raw_git_diffs.iter_mut().find(|item| {
                item.file_name
                    == delta
                        .new_file()
                        .path()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .to_owned()
            }) {
                Some(git_diff_item) => match line.origin_value() {
                    git2::DiffLineType::Context => {
                        git_diff_item.diff.push(GitDiff {
                            action: git2::DiffLineType::Context,
                            text: std::str::from_utf8(line.content())
                                .unwrap_or_default()
                                .to_owned(),
                        });
                    }
                    git2::DiffLineType::Addition => {
                        git_diff_item.diff.push(GitDiff {
                            action: git2::DiffLineType::Addition,
                            text: std::str::from_utf8(line.content())
                                .unwrap_or_default()
                                .to_owned(),
                        });
                    }
                    git2::DiffLineType::Deletion => {
                        git_diff_item.diff.push(GitDiff {
                            action: git2::DiffLineType::Deletion,
                            text: std::str::from_utf8(line.content())
                                .unwrap_or_default()
                                .to_owned(),
                        });
                    }
                    _ => {}
                },
                None => {}
            }

            true
        };

        diff.foreach(
            &mut |_: git2::DiffDelta<'_>, _: f32| true,
            None,
            None,
            Some(&mut line_callback),
        )
        .expect("Failed to iterate through diff entries");

        break;
    }

    // println!("{:?}", raw_git_diffs);

    let parsed_git_diffs: Vec<ParsedGitDiff> = parse_raw_git_diffs(raw_git_diffs);
    // println!("{:?}", parsed_git_diffs);

    Ok(parsed_git_diffs)
}

fn parse_raw_git_diffs(raw_git_diffs: Vec<RawGitDiff>) -> Vec<ParsedGitDiff> {
    raw_git_diffs
        .into_iter()
        .map(|raw_git_diff| {
            let mut new_code = Vec::new();
            let mut old_code = Vec::new();

            for git_diff in raw_git_diff.diff.iter() {
                match git_diff.action {
                    git2::DiffLineType::Context => {
                        old_code.push(git_diff.text.clone());
                        new_code.push(git_diff.text.clone());
                    }
                    git2::DiffLineType::Addition => {
                        new_code.push(git_diff.text.clone());
                    }
                    git2::DiffLineType::Deletion => {
                        old_code.push(git_diff.text.clone());
                    }
                    _ => {}
                }
            }

            ParsedGitDiff {
                file_name: raw_git_diff.file_name,
                status: raw_git_diff.status,
                old: old_code,
                new: new_code,
            }
        })
        .collect()
}

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Post {
    model: HashMap<String, String>,
    messages: Vec<HashMap<String, String>>,
    key: String,
    prompt: String,
    temperature: f32,
}

impl Post {
    fn new(messages: Vec<HashMap<String, String>>) -> Self {
        Post {
            model: HashMap::from([
                (String::from("id"), String::from("openchat_v3.2_mistral")),
                (String::from("name"), String::from("OpenChat Aura")),
                (String::from("maxLength"), String::from("30000")),
                (String::from("tokenLimit"), String::from("8192")),
            ]),
            messages: messages,
            key: String::from(""),
            prompt: String::from(" "),
            temperature: 0.6,
        }
    }
}

impl Default for Post {
    fn default() -> Self {
        Post {
            model: HashMap::from([
                (String::from("id"), String::from("openchat_v3.2_mistral")),
                (String::from("name"), String::from("OpenChat Aura")),
                (String::from("maxLength"), String::from("30000")),
                (String::from("tokenLimit"), String::from("8192")),
            ]),
            messages: Vec::new(),
            key: String::from(""),
            prompt: String::from(" "),
            temperature: 0.3,
        }
    }
}

async fn post_request(commit_diff_files: Vec<ParsedGitDiff>) -> Result<()> {
    let client = Client::new();
    let url = "https://openchat.team/api/chat";

    let bodies = stream::iter(commit_diff_files)
        .map(|file_diff| {
            let client = &client;

            let promt_template = format!("You are a technical writer for a software team. Your job is to generate CHANGELOG type summary.\nBased on the following context about the 'file_name', 'file_status', 'old_code_block', and 'new_code_block', generate a short, technical and to-the-point summary of the changes made.\n\n----- Context -----\nfile_name: {file_name}\nfile_status: {file_status:?}\nold_code_block: {old_code_block}\nnew_code_block: {new_code_block}\n------------\n", file_name=file_diff.file_name, file_status=file_diff.status, old_code_block=file_diff.old.join("\n"), new_code_block=file_diff.new.join("\n"));

            let payload = Post::new(vec![HashMap::from([
                (String::from("role"), String::from("user")),
                (String::from("content"), promt_template),
            ])]);
            async move {
                let resp = client.post(url).json(&payload).send().await?;
                resp.text().await
            }
        })
        .buffered(CONCURRENT_REQUESTS);

    bodies
        .for_each(|b| async { println!("Response: {:#?}", b.unwrap()) })
        .await;

    Ok(())
}
