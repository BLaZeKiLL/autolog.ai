AutoGitLog

Assumptions:
    - Your `.git` folder should be in the root the project.
    - Running this program only `on_merge`.

```
$ git log \
$(git merge-base --octopus \
$(git log -1 \
--merges --pretty=format:%P))..$(git log -1 --merges --pretty=format:%H) \
--boundary --graph --pretty=oneline --abbrev-commit
```

How to use:
```
$ cargo run -- <path_to_project> generate
```